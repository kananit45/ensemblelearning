# -*- coding: utf-8 -*-
u"""決定木を作る."""
import numpy as np


def gini_index(labels):
	u"""ジニ係数."""
	c_given_t = []
	for l in np.unique(labels):
		c_given_t.append(np.sum(labels == l))
	c_given_t = np.array(c_given_t, dtype=float) / len(labels)
	return 1 - np.sum(c_given_t**2)


class DecisionTree(object):
	u"""決定木アルゴリズム."""

	def __init__(self, maxdepth=10):
		u"""初期化."""
		self.data = []
		self.target = []
		self.root = []
		self.maxdepth = maxdepth

	def fit(self, data, target):
		u"""モデル当てはめ."""
		n_, d_ = data.shape
		self.data = data
		self.target = target
		member_id = np.arange(n_)
		self.root = TreeNode(member_id, 1, self.get_data)
		self.root.search_boundary(self.maxdepth)

	def predict(self, test):
		u"""ラベルの推定."""
		nt_, d_ = test.shape
		p_label = np.zeros(nt_)
		test_id = np.arange(nt_)

		def set_label(member, label):
			u"""memberにlabelを代入する."""
			p_label[member] = label

		def get_test(member):
			u"""ノードからテストデータを取得する関数."""
			return test[member, :]

		self.root.labeling(test_id, get_test, set_label)
		return p_label

	def get_data(self, index):
		u"""ノードがメンバーを取得するための関数."""
		return (self.data[index, :], self.target[index])


class TreeNode(object):
	u"""決定木ノード."""

	def __init__(self, member, depth, getter):
		u"""初期化."""
		self.member = member      # 自分に属するデータ
		self.depth = depth        # 木の深さ
		self.majority = None      # ノードの多数派クラス(葉のみ)
		self.left = None          # 左のノード
		self.right = None         # 右のノード
		self.div_dim = None       # 分割する次元
		self.boundary = None      # 分割境界
		self.get_member = getter  # Treeクラスからデータを取得するための関数

	def search_boundary(self, maxdepth):
		u"""分割境界を決定する."""
		feature, target = self.get_member(self.member)
		n_, d_ = feature.shape
		original = gini_index(target)          # 元の不純度
		if original == 0:                      # 不純度が0なら終了
			self.majority = target[0]
			return
		if self.depth == maxdepth:
			inlabel = np.unique(target)
			count = [np.sum(c == target) for c in inlabel]
			self.majority = inlabel[np.argmax(count)]
			return

		d_best = []                 # 特徴軸ごとのbest
		for d in xrange(d_):
			gain = []                            # 不純度の減少量
			d_cand = np.unique(feature[:, d])    # 閾値候補
			if len(d_cand) != 1:
				d_cand = (d_cand[:-1] + d_cand[1:]) / 2.     # 分割が存在したら
			else:
				d_cand = d_cand                # 値が完全一致していたら自分自身とする

			for div in d_cand:
				# 左に条件が偽, 右に真を割り振る.
				left = feature[:, d] < div
				right = feature[:, d] >= div
				p_l = 1. * np.sum(left) / n_              # 左の割合
				p_r = 1. * np.sum(right) / n_             # 右の割合
				gain.append(
					original
					- p_l * gini_index(target[left]) - p_r * gini_index(target[right])
				)
			maxgain = np.argmax(gain)
			boundary = d_cand[maxgain]         # d次元目での最良境界
			max_gain = gain[maxgain]           # d次元目での不純度減少量の最大値
			d_best.append((max_gain, boundary))

		best = np.argmax(np.array(d_best)[:, 0])    # 最良分割次元
		boundary = d_best[best][1]                  # 分割境界

		if np.sum(feature[:, best] < boundary) == 0 or np.sum(feature[:, best] >= boundary) == 0:
			inlabel = np.unique(target)
			count = [np.sum(c == target) for c in inlabel]
			self.majority = inlabel[np.argmax(count)]
			return

		self.left = TreeNode(
			self.member[feature[:, best] < boundary],
			self.depth + 1,
			self.get_member
		)
		self.right = TreeNode(
			self.member[feature[:, best] >= boundary],
			self.depth + 1,
			self.get_member
		)

		self.div_dim = best
		self.boundary = boundary

		self.left.search_boundary(maxdepth)        # 左ノードの分割境界を決める
		self.right.search_boundary(maxdepth)       # 右ノードの分割境界を決める

	def labeling(self, test_member, getter, setter):
		u"""未知データに対するラベリング."""
		boundary = self.boundary
		if boundary is None:               # これ以上の分割がなければ多数派でラベリング
			setter(test_member, self.majority)
			return
		else:
			test = getter(test_member)     # 未知データの特徴量
			self.left.labeling(
				test_member[test[:, self.div_dim] < boundary],
				getter, setter
			)
			self.right.labeling(
				test_member[test[:, self.div_dim] >= boundary],
				getter, setter
			)
































