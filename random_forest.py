# -*- coding: utf-8 -*-
u"""決定木を使ったBaggingの実装."""
import numpy as np
import random
from decision_tree import DecisionTree
from decision_tree import TreeNode
from decision_tree import gini_index


class RandomForest(object):
	u"""ランダムフォレスト."""

	def __init__(self, bootstrap=0.5, n_tree=10,
		maxdepth=np.inf, d_mask=0.2, seed=None):
		u"""
		初期化.

		Attribute :
		n_tree    : 構成する木の数
		bootstrap : 訓練データに対するブートストラップサンプルの割合
		d_ratio   : maskingする特徴の割合
		maxdepth  : 木の最大深さ
		"""
		self.n_tree = n_tree
		self.maxdepth = maxdepth
		self.d_mask = d_mask
		self.bootstrap = bootstrap
		self.forest = []            # 決定木の集合
		self.seed = seed            # ブートストラップするときのseed

	def fit(self, data, target):
		u"""決定木の当てはめ."""
		n, d = data.shape
		random.seed(self.seed)
		for i in xrange(self.n_tree):
			sample = []
			for j in range(int(n * self.bootstrap)):
				subsample = random.choice(range(n))
				sample.append(subsample)
			dtree = RandomTree(self.maxdepth, self.d_mask)
			dtree.fit(data[sample, :], target[sample])    # 決定木を学習
			self.forest.append(dtree)                     # 学習した木を森に追加

	def predict(self, testdata):
		u"""予測."""
		p_labels = []
		for tree in self.forest:
			p_labels.append(tree.predict(testdata))
		p_labels = np.array(p_labels)
		prediction = []
		for k_tree in p_labels.T:
			kind = np.unique(k_tree)
			vote = [np.sum(k_tree == k) for k in kind]
			prediction.append(kind[np.argmax(vote)])
		return np.array(prediction)


class RandomTree(DecisionTree):
	u"""ランダムフォレストを構成する決定木."""

	def __init__(self, maxdepth, mask_ratio):
		u"""
		初期化.

		mask_ratio : マスクする特徴の割合
		others     : c.f. <class> DecisionTree
		"""
		if mask_ratio >= 1 or mask_ratio < 0:
			print "parameter \"mask_ratio\" must be real number from 0 to 1."
			return
		self.mask_ratio = mask_ratio
		super(RandomTree, self).__init__(maxdepth)

	def fit(self, data, target):
		u"""モデル当てはめ."""
		n_, d_ = data.shape
		self.data = data
		self.target = target
		member_id = np.arange(n_)
		self.root = RandomMaskNode(member_id, 1, self.get_data)
		self.root.search_boundary(self.maxdepth, self.mask_ratio)


class RandomMaskNode(TreeNode):
	u"""決定境界を定めるときに一部の特徴をマスクする機能を備えた木ノード."""

	def search_boundary(self, maxdepth, mask_ratio):
		u"""分割境界を決定する."""
		feature, target = self.get_member(self.member)
		n_, d_ = feature.shape
		original = gini_index(target)          # 元の不純度
		if original == 0:                      # 不純度が0なら終了
			self.majority = target[0]
			return
		if self.depth == maxdepth:
			inlabel = np.unique(target)
			count = [np.sum(c == target) for c in inlabel]
			self.majority = inlabel[np.argmax(count)]
			return

		d_best = {}                 # 特徴軸ごとのbest
		sample_dim = np.array(random.sample(xrange(d_), int((1 - mask_ratio) * d_)))
		for d in sample_dim:
			gain = []                            # 不純度の減少量
			d_cand = np.unique(feature[:, d])    # 閾値候補
			if len(d_cand) != 1:
				d_cand = (d_cand[:-1] + d_cand[1:]) / 2.     # 分割が存在したら
			else:
				d_cand = d_cand                # 値が完全一致していたら自分自身とする

			for div in d_cand:
				# 左に条件が偽, 右に真を割り振る.
				left = feature[:, d] < div
				right = feature[:, d] >= div
				p_l = 1. * np.sum(left) / n_              # 左の割合
				p_r = 1. * np.sum(right) / n_             # 右の割合
				gain.append(
					original
					- p_l * gini_index(target[left]) - p_r * gini_index(target[right])
				)
			maxgain = np.argmax(gain)
			boundary = d_cand[maxgain]         # d次元目での最良境界
			max_gain = gain[maxgain]           # d次元目での不純度減少量の最大値
			# d_best.append((max_gain, boundary))
			d_best[d] = [max_gain, boundary]

		# best = sample_dim[np.argmax(np.array(d_best)[:, 0])]    # 最良分割次元
		best_dim = None
		best_gain = -np.inf
		boundary = None
		for key in d_best.keys():
			gain, tmp_bound = d_best[key]
			if best_gain < gain:
				best_dim = key
				best_gain = gain
				boundary = tmp_bound

		# print "best dim : {}".format(best_dim)
		# print "d_best   : \n{}".format(d_best)
		# boundary = d_best[best][1]                  # 分割境界

		if np.sum(feature[:, best_dim] < boundary) == 0 or np.sum(feature[:, best_dim] >= boundary) == 0:
			# print "aaa"
			inlabel = np.unique(target)
			count = [np.sum(c == target) for c in inlabel]
			self.majority = inlabel[np.argmax(count)]
			return

		# print "bbbb"
		self.left = RandomMaskNode(
			self.member[feature[:, best_dim] < boundary],
			self.depth + 1,
			self.get_member
		)
		self.right = RandomMaskNode(
			self.member[feature[:, best_dim] >= boundary],
			self.depth + 1,
			self.get_member
		)

		self.div_dim = best_dim
		self.boundary = boundary

		self.left.search_boundary(maxdepth, mask_ratio)        # 左ノードの分割境界を決める
		self.right.search_boundary(maxdepth, mask_ratio)       # 右ノードの分割境界を決める


























