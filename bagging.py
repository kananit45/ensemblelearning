# -*- coding: utf-8 -*-
u"""決定木を使ったBaggingの実装."""
import numpy as np
import random
from decision_tree import DecisionTree


class Bagging(object):
	u"""ブートストラップサンプリングによる決定木のアンサンブル学習."""

	def __init__(self, bootstrap=0.5, n_tree=10, maxdepth=3, seed=None):
		u"""
		初期化.

		Attribute :
		n_tree    : 構成する木の数
		bootstrap : 訓練データに対するブートストラップサンプルの割合
		maxdepth  : 木の最大深さ
		"""
		self.n_tree = n_tree
		self.maxdepth = maxdepth
		self.bootstrap = bootstrap
		self.forest = []          # 決定木の集合
		self.seed = seed            # ブートストラップするときのseed

	def fit(self, data, target):
		u"""決定木の当てはめ."""
		n, d = data.shape
		random.seed(self.seed)
		for i in xrange(self.n_tree):
			sample = []
			for j in range(int(n * self.bootstrap)):
				subsample = random.choice(range(n))
				sample.append(subsample)
			dtree = DecisionTree(self.maxdepth)
			dtree.fit(data[sample, :], target[sample])    # 決定木を学習
			self.forest.append(dtree)                     # 学習した木を森に追加

	def predict(self, testdata):
		u"""予測."""
		p_labels = []
		for tree in self.forest:
			p_labels.append(tree.predict(testdata))
		p_labels = np.array(p_labels)
		prediction = []
		for k_tree in p_labels.T:
			kind = np.unique(k_tree)
			vote = [np.sum(k_tree == k) for k in kind]
			prediction.append(kind[np.argmax(vote)])
		return np.array(prediction)
























